package io.sample.inventory.flow.stock.domain.logic;

import io.naraway.accent.domain.type.IdName;
import io.sample.inventory.aggregate.stock.domain.entity.sdo.StockCdo;
import io.sample.inventory.aggregate.stock.domain.logic.StockLogic;
import io.sample.inventory.aggregate.stock.domain.logic.StockUserLogic;
import io.naraway.product.event.product.ProductDomainEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@RequiredArgsConstructor
public class StockHandlerLogic {
    //
    private final StockLogic stockLogic;
    private final StockUserLogic stockUserLogic;
    public void onProduct(ProductDomainEvent event) {
        //
        log.debug("onProductEvent : \n{}", event.toString());
        switch (event.getEventType()) {
            case Registered:
                newStock(event);
                break;
            case Modified:
                break;
            case Removed:
                break;
        }
    }

    private void newStock(ProductDomainEvent event) {
        //
        StockCdo cdo = new StockCdo(event.getActorKey(),
                IdName.of(event.getId(), event.getName()),
                0);

        stockLogic.registerStock(cdo);
        log.debug("Stock registered\n{}", cdo);
    }

//    public void onSold(ProductSoldEvent event) {
//        //
//        stockUserLogic.decrease(event.getProductId(), event.getQuantity());
//    }
//
//    public void onOrderCanceled(OrderCanceledEvent event) {
//        //
//        stockUserLogic.increase(event.getProductId(), event.getQuantity());
//    }
}
