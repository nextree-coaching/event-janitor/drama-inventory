package io.sample.inventory.facade.event.listener.domain;

import io.sample.inventory.flow.stock.domain.logic.StockHandlerLogic;
import io.naraway.janitor.event.EventHandler;
import io.naraway.product.event.product.ProductDomainEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class ProductDomainEventHandler {
    //
    private final StockHandlerLogic stockHandlerLogic;

    @EventHandler
    public void handle(ProductDomainEvent event) {
        //
        stockHandlerLogic.onProduct(event);
    }
}
