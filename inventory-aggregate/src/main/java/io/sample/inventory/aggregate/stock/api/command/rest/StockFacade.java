/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.api.command.rest;

import io.naraway.accent.domain.message.api.command.CommandResponse;
import io.sample.inventory.aggregate.stock.api.command.command.StockCommand;

public interface StockFacade {
    /* Autogen by nara studio */
    CommandResponse executeStock(StockCommand stockCommand);
}
