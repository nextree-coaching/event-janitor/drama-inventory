/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.api.query.query;

import io.sample.inventory.aggregate.stock.domain.entity.Stock;
import io.sample.inventory.aggregate.stock.store.StockStore;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.query.CqrsBaseQuery;

@Getter
@Setter
@NoArgsConstructor
public class StockQuery extends CqrsBaseQuery<Stock> {
    /* Autogen by nara studio */
    private String stockId;

    public void execute(StockStore stockStore) {
        /* Autogen by nara studio */
        setQueryResult(stockStore.retrieve(stockId));
    }
}
