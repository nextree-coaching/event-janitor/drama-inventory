/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.api.command.command;

import io.sample.inventory.aggregate.stock.domain.entity.sdo.StockCdo;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.command.CqrsBaseCommand;

import java.util.List;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.message.api.command.CqrsBaseCommandType;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class StockCommand extends CqrsBaseCommand {
    //
    private StockCdo stockCdo;
    private List<StockCdo> stockCdos;
    private boolean multiCdo;
    private String stockId;
    private NameValueList nameValues;

    protected StockCommand(CqrsBaseCommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static StockCommand newRegisterStockCommand(StockCdo stockCdo) {
        /* Autogen by nara studio */
        StockCommand command = new StockCommand(CqrsBaseCommandType.Register);
        command.setStockCdo(stockCdo);
        return command;
    }

    public static StockCommand newRegisterStockCommand(List<StockCdo> stockCdos) {
        /* Autogen by nara studio */
        StockCommand command = new StockCommand(CqrsBaseCommandType.Register);
        command.setStockCdos(stockCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static StockCommand newModifyStockCommand(String stockId, NameValueList nameValues) {
        /* Autogen by nara studio */
        StockCommand command = new StockCommand(CqrsBaseCommandType.Modify);
        command.setStockId(stockId);
        command.setNameValues(nameValues);
        return command;
    }

    public static StockCommand newRemoveStockCommand(String stockId) {
        /* Autogen by nara studio */
        StockCommand command = new StockCommand(CqrsBaseCommandType.Remove);
        command.setStockId(stockId);
        return command;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static StockCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, StockCommand.class);
    }

    public static StockCommand sampleForRegister() {
        /* Autogen by nara studio */
        return newRegisterStockCommand(StockCdo.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sampleForRegister());
    }
}
