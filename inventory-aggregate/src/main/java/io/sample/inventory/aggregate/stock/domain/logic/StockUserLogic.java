package io.sample.inventory.aggregate.stock.domain.logic;

import io.sample.inventory.aggregate.stock.domain.entity.Stock;
import io.sample.inventory.aggregate.stock.store.StockStore;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class StockUserLogic {
    //
    private final StockStore stockStore;
    private final StockLogic stockLogic;

    public int decrease(String stockId, int amount) {
        //
        Stock stock = stockLogic.findStock(stockId);
        if(stock.getQuantity() - amount >= 0) {
            stock.setQuantity(stock.getQuantity() - amount);
            stockStore.update(stock);
            return stock.getQuantity();
        }

        throw new RuntimeException(String.format("Not enough stock for %s", stock.getProductName()));
    }

    public int increase(String stockId, int amount) {
        //
        Stock stock = stockLogic.findStock(stockId);
        stock.setQuantity(stock.getQuantity() + amount);
        stockStore.update(stock);
        return stock.getQuantity();
    }
}
