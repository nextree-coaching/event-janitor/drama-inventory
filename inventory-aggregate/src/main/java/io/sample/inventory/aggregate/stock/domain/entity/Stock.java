/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.domain.entity;

import io.naraway.accent.domain.ddd.GenOptions;
import io.sample.inventory.aggregate.stock.domain.entity.sdo.StockCdo;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.ddd.StageEntity;
import io.naraway.accent.domain.ddd.DomainAggregate;
import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import org.springframework.beans.BeanUtils;
import io.naraway.accent.domain.type.NameValue;

/*
ex)
@GenerateOptions(
    updatable = { "name" },
    properties = {
        @RdbProperty(name = "name", columnName = "col_name", columnType = "varchar(255)", columnNullable = true)
    })
*/
@Getter
@Setter
@NoArgsConstructor
@GenOptions(updatable = {"quantity"})
public class Stock extends StageEntity implements DomainAggregate {
    //
    private String productName;
    private int quantity;

    public Stock(String id, ActorKey actorKey) {
        //
        super(id, actorKey);
    }

    public Stock(StockCdo stockCdo) {
        //
        super(stockCdo.getActorKey());
        BeanUtils.copyProperties(stockCdo, this);
    }

    public static Stock newInstance(StockCdo stockCdo, NameValueList nameValueList) {
        //
        Stock stock = new Stock(stockCdo);
        stock.modifyAttributes(nameValueList);
        return stock;
    }

    public String toString() {
        //
        return toJson();
    }

    public static Stock fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Stock.class);
    }

    public static Stock sample() {
        //
        return new Stock(StockCdo.sample());
    }

    @Override
    protected void modifyAttributes(NameValueList nameValues) {
        //
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch(nameValue.getName()) {
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public NameValueList genNameValueList() {
        //
        NameValueList nameValueList = NameValueList.newEmptyInstance();
        return nameValueList;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
