/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.store;

import io.sample.inventory.aggregate.stock.domain.entity.Stock;

public interface StockStore {
    /* Autogen by nara studio */
    void create(Stock stock);
    Stock retrieve(String id);
    void update(Stock stock);
    void delete(Stock stock);
    void delete(String id);
    boolean exists(String id);
}
