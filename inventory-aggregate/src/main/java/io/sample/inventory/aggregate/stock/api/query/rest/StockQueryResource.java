/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.api.query.rest;

import io.sample.inventory.aggregate.stock.api.query.query.StockDynamicQuery;
import io.sample.inventory.aggregate.stock.api.query.query.StockQuery;
import io.sample.inventory.aggregate.stock.api.query.query.StocksDynamicQuery;
import io.sample.inventory.aggregate.stock.domain.entity.Stock;
import io.sample.inventory.aggregate.stock.store.StockStore;
import io.sample.inventory.aggregate.stock.store.maria.jpo.StockJpo;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.naraway.accent.util.query.RdbQueryRequest;

import javax.persistence.EntityManager;
import io.naraway.accent.domain.message.api.query.QueryResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@RestController
@RequestMapping("/aggregate/stock/stock/query")
public class StockQueryResource implements StockQueryFacade {
    //
    private final StockStore stockStore;
    private final RdbQueryRequest<StockJpo> request;

    public StockQueryResource(StockStore stockStore, EntityManager entityManager) {
        /* Autogen by nara studio */
        this.stockStore = stockStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QueryResponse<Stock> execute(@RequestBody StockQuery stockQuery) {
        /* Autogen by nara studio */
        stockQuery.execute(stockStore);
        return stockQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-single")
    public QueryResponse<Stock> execute(@RequestBody StockDynamicQuery stockDynamicQuery) {
        /* Autogen by nara studio */
        stockDynamicQuery.execute(request);
        return stockDynamicQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QueryResponse<List<Stock>> execute(@RequestBody StocksDynamicQuery stocksDynamicQuery) {
        /* Autogen by nara studio */
        stocksDynamicQuery.execute(request);
        return stocksDynamicQuery.getQueryResponse();
    }
}
