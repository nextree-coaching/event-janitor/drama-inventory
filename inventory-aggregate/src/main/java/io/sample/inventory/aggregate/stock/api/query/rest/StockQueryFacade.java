/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.api.query.rest;

import io.naraway.accent.domain.message.api.query.QueryResponse;
import io.sample.inventory.aggregate.stock.domain.entity.Stock;
import io.sample.inventory.aggregate.stock.api.query.query.StockQuery;
import io.sample.inventory.aggregate.stock.api.query.query.StockDynamicQuery;
import java.util.List;
import io.sample.inventory.aggregate.stock.api.query.query.StocksDynamicQuery;

public interface StockQueryFacade {
    /* Autogen by nara studio */
    QueryResponse<Stock> execute(StockQuery stockQuery);
    QueryResponse<Stock> execute(StockDynamicQuery stockDynamicQuery);
    QueryResponse<List<Stock>> execute(StocksDynamicQuery stocksDynamicQuery);
}
