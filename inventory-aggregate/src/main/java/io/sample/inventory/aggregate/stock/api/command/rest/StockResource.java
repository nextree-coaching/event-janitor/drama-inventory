/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.api.command.rest;

import io.sample.inventory.aggregate.stock.api.command.command.StockCommand;
import io.sample.inventory.aggregate.stock.domain.logic.StockLogic;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.naraway.accent.domain.message.api.command.CommandResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/aggregate/stock")
public class StockResource implements StockFacade {
    /* Autogen by nara studio */
    private final StockLogic stockLogic;

    public StockResource(StockLogic stockLogic) {
        /* Autogen by nara studio */
        this.stockLogic = stockLogic;
    }

    @Override
    @PostMapping("/stock/command")
    public CommandResponse executeStock(@RequestBody StockCommand stockCommand) {
        /* Autogen by nara studio */
        return stockLogic.routeCommand(stockCommand).getCommandResponse();
    }
}
