/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.store.maria.jpo;

import io.sample.inventory.aggregate.stock.domain.entity.Stock;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import io.naraway.accent.store.jpa.StageEntityJpo;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "STOCK")
public class StockJpo extends StageEntityJpo {
    //
    private String productName; //
    private int quantity;

    public StockJpo(Stock stock) {
        /* Autogen by nara studio */
        super(stock);
        BeanUtils.copyProperties(stock, this);
    }

    public Stock toDomain() {
        /* Autogen by nara studio */
        Stock stock = new Stock(getId(), genActorKey());
        BeanUtils.copyProperties(this, stock);
        return stock;
    }

    public static List<Stock> toDomains(List<StockJpo> stockJpos) {
        /* Autogen by nara studio */
        return stockJpos.stream().map(StockJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static StockJpo sample() {
        /* Autogen by nara studio */
        return new StockJpo(Stock.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample());
    }
}
