/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.store.maria;

import io.sample.inventory.aggregate.stock.domain.entity.Stock;
import io.sample.inventory.aggregate.stock.store.maria.jpo.StockJpo;
import io.sample.inventory.aggregate.stock.store.maria.repository.StockMariaRepository;
import org.springframework.stereotype.Repository;
import io.sample.inventory.aggregate.stock.store.StockStore;

import java.util.Optional;
import org.springframework.data.domain.Pageable;
import io.naraway.accent.domain.type.Offset;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Repository
public class StockMariaStore implements StockStore {
    /* Autogen by nara studio */
    private final StockMariaRepository stockMariaRepository;

    public StockMariaStore(StockMariaRepository stockMariaRepository) {
        /* Autogen by nara studio */
        this.stockMariaRepository = stockMariaRepository;
    }

    @Override
    public void create(Stock stock) {
        /* Autogen by nara studio */
        StockJpo stockJpo = new StockJpo(stock);
        stockMariaRepository.save(stockJpo);
    }

    @Override
    public Stock retrieve(String id) {
        /* Autogen by nara studio */
        Optional<StockJpo> stockJpo = stockMariaRepository.findById(id);
        return stockJpo.map(StockJpo::toDomain).orElse(null);
    }

    @Override
    public void update(Stock stock) {
        /* Autogen by nara studio */
        StockJpo stockJpo = new StockJpo(stock);
        stockMariaRepository.save(stockJpo);
    }

    @Override
    public void delete(Stock stock) {
        /* Autogen by nara studio */
        stockMariaRepository.deleteById(stock.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        stockMariaRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return stockMariaRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
