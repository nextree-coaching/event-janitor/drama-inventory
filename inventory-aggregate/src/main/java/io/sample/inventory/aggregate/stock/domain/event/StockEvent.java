/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.domain.event;

import io.sample.inventory.aggregate.stock.domain.entity.Stock;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.event.CqrsDataEvent;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.message.event.CqrsDataEventType;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class StockEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private Stock stock;
    private String stockId;
    private NameValueList nameValues;

    protected StockEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static StockEvent newStockRegisteredEvent(Stock stock, String stockId) {
        /* Autogen by nara studio */
        StockEvent event = new StockEvent(CqrsDataEventType.Registered);
        event.setStock(stock);
        event.setStockId(stockId);
        return event;
    }

    public static StockEvent newStockModifiedEvent(String stockId, NameValueList nameValues, Stock stock) {
        /* Autogen by nara studio */
        StockEvent event = new StockEvent(CqrsDataEventType.Modified);
        event.setStockId(stockId);
        event.setNameValues(nameValues);
        event.setStock(stock);
        return event;
    }

    public static StockEvent newStockRemovedEvent(Stock stock, String stockId) {
        /* Autogen by nara studio */
        StockEvent event = new StockEvent(CqrsDataEventType.Removed);
        event.setStock(stock);
        event.setStockId(stockId);
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static StockEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, StockEvent.class);
    }
}
