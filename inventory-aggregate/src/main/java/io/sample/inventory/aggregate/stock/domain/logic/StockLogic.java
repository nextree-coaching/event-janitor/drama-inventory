/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory.aggregate.stock.domain.logic;

import io.sample.inventory.aggregate.stock.domain.entity.Stock;
import io.sample.inventory.aggregate.stock.domain.entity.sdo.StockCdo;
import io.sample.inventory.aggregate.stock.domain.event.StockEvent;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import io.sample.inventory.aggregate.stock.store.StockStore;
import io.naraway.janitor.EventStream;
import io.sample.inventory.aggregate.stock.api.command.command.StockCommand;
import io.naraway.accent.domain.message.api.command.CommandResponse;
import io.naraway.accent.domain.message.api.FailureMessage;
import java.util.Optional;

import io.naraway.accent.domain.type.NameValueList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.NoSuchElementException;

@Service
@Transactional
public class StockLogic {
    //
    private final StockStore stockStore;
    private final EventStream eventStream;

    public StockLogic(StockStore stockStore, EventStream eventStream) {
        /* Autogen by nara studio */
        this.stockStore = stockStore;
        this.eventStream = eventStream;
    }

    public StockCommand routeCommand(StockCommand command) {
        /* Autogen by nara studio */
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = this.registerStocks(command.getStockCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = Optional.ofNullable(command.getNameValues()).filter(nameValueList -> command.getNameValues().size() != 0).map(nameValueList -> this.registerStock(command.getStockCdo(), nameValueList)).orElse(this.registerStock(command.getStockCdo()));
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyStock(command.getStockId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getStockId()));
                break;
            case Remove:
                this.removeStock(command.getStockId());
                command.setCommandResponse(new CommandResponse(command.getStockId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerStock(StockCdo stockCdo) {
        /* Autogen by nara studio */
        Stock stock = new Stock(stockCdo);
        if (stockStore.exists(stock.getId())) {
            throw new IllegalArgumentException("stock already exists. " + stock.getId());
        }
        stockStore.create(stock);
        StockEvent stockEvent = StockEvent.newStockRegisteredEvent(stock, stock.getId());
        eventStream.publishEvent(stockEvent);
        return stock.getId();
    }

    public String registerStock(StockCdo stockCdo, NameValueList nameValueList) {
        /* Autogen by nara studio */
        Stock stock = Stock.newInstance(stockCdo, nameValueList);
        if (stockStore.exists(stock.getId())) {
            throw new IllegalArgumentException("stock already exists. " + stock.getId());
        }
        stockStore.create(stock);
        StockEvent stockEvent = StockEvent.newStockRegisteredEvent(stock, stock.getId());
        eventStream.publishEvent(stockEvent);
        return stock.getId();
    }

    public List<String> registerStocks(List<StockCdo> stockCdos) {
        /* Autogen by nara studio */
        return stockCdos.stream().map(stockCdo -> this.registerStock(stockCdo)).collect(Collectors.toList());
    }

    public Stock findStock(String stockId) {
        /* Autogen by nara studio */
        Stock stock = stockStore.retrieve(stockId);
        if (stock == null) {
            throw new NoSuchElementException("Stock id: " + stockId);
        }
        return stock;
    }

    public void modifyStock(String stockId, NameValueList nameValues) {
        /* Autogen by nara studio */
        Stock stock = findStock(stockId);
        stock.modify(nameValues);
        stockStore.update(stock);
        StockEvent stockEvent = StockEvent.newStockModifiedEvent(stockId, nameValues, stock);
        eventStream.publishEvent(stockEvent);
    }

    public void removeStock(String stockId) {
        /* Autogen by nara studio */
        Stock stock = findStock(stockId);
        stockStore.delete(stock);
        StockEvent stockEvent = StockEvent.newStockRemovedEvent(stock, stock.getId());
        eventStream.publishEvent(stockEvent);
    }

    public boolean existsStock(String stockId) {
        /* Autogen by nara studio */
        return stockStore.exists(stockId);
    }

    public void handleEventForProjection(StockEvent stockEvent) {
        /* Autogen by nara studio */
        switch(stockEvent.getCqrsDataEventType()) {
            case Registered:
                stockStore.create(stockEvent.getStock());
                break;
            case Modified:
                Stock stock = stockStore.retrieve(stockEvent.getStockId());
                stock.modify(stockEvent.getNameValues());
                stockStore.update(stock);
                break;
            case Removed:
                stockStore.delete(stockEvent.getStockId());
                break;
        }
    }
}
