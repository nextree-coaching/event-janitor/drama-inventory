/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */

package io.sample.inventory;

import io.naraway.accent.domain.key.kollection.DramaRole;
import io.naraway.accent.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.List;

@SuppressWarnings("java:S115")
@Getter
@Setter
@NoArgsConstructor
public class InventoryDrama {
    //
    public static final String StationAdmin = "station-admin";

    private String id;
    private String name;
    private String version;
    private List<DramaRole> roles;

    public static InventoryDrama fromJson(String json) {
        //
        InventoryDrama drama = JsonUtil.fromJson(json, InventoryDrama.class);
        drama.validate();
        drama.getRoles().forEach(role -> role.setDramaId(drama.getId()));

        return drama;
    }

    public void validate() {
        //
        Assert.hasText(this.id, "'id' is required");
        Assert.hasText(this.name, "'name' is required");
        Assert.hasText(this.version, "'version' is required");
        Assert.notNull(this.roles, "'roles' is required");

        if (roles.stream().noneMatch(role -> role.getCode().equals(StationAdmin))) {
            throw new IllegalArgumentException("drama role is missed, role = " + StationAdmin);
        }
    }
}
