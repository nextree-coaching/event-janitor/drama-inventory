/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.inventory;

import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import io.naraway.drama.prologue.spacekeeper.config.DramaApplication;
import io.naraway.drama.prologue.rolekeeper.config.EnableDramaRoleBaseAccess;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.SpringApplication;

@EnableSwagger2
@DramaApplication
@EnableDramaRoleBaseAccess
@SpringBootApplication(scanBasePackages = { "io.naraway.inventory" }, exclude = MongoAutoConfiguration.class)
@EnableJpaRepositories(basePackages = { "io.naraway.inventory" })
@EntityScan(basePackages = { "io.naraway.inventory" })
public class InventoryBootApplication {
    //

    public static void main(String[] args) {
        /* Autogen by nara studio */
        SpringApplication.run(InventoryBootApplication.class, args);
    }
}
